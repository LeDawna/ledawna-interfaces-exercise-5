﻿
namespace Desktop_Exercise_5.Utility
{
  public static class Lookups
  {
    /// <summary>
    /// Single, Team, Unknown
    /// </summary>
    public enum SportCategory
    {
      Single,
      Team,
      Unknown
    }
  }
}
