﻿
namespace Desktop_Exercise_5.Interfaces
{
  public interface ICourt
  {
    /// <summary>
    /// length of the court
    /// </summary>
    decimal Length { get; set; }

    /// <summary>
    /// width of the court
    /// </summary>
    decimal Width { get; set; }
  }
}
