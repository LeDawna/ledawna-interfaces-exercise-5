﻿
namespace Desktop_Exercise_5.Interfaces
{
  public interface IBall
  {
    /// <summary>
    /// number of players on the team
    /// </summary>
    int NumberOfPlayers { get; set; }

    /// <summary>
    /// name of the sport
    /// </summary>
    string SportName { get; }

    /// <summary>
    /// write all sport properties to the console
    /// </summary>
    void WriteSportProperties();
  }
}
